/**
* Assets collection for accessing MongoDB colleciton, accessing and storing documents
* See http://docs.meteor.com for details on collection methods
*
* @class Assets
*/
Assets = new Meteor.Collection("assets");
