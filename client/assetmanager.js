// Functions for managing upload
function uploadProgress(evt) {
  // if (evt.lengthComputable) {
  //   var percentComplete = Math.round(evt.loaded * 100 / evt.total);
  //   document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
  // }
  // else {
  //   document.getElementById('progressNumber').innerHTML = 'unable to compute';
  // }
}

function uploadFailed(evt) { // Basic alert for errors
  alert("There was an error attempting to upload the file." + evt);
}

function uploadCanceled(evt) { // Basic alert for errors
  alert("The upload has been canceled by the user or the browser dropped the connection.");
}

/**
* Primary layout functionality
*
* @class Layout
*/

/**
* Provide links for the menu
*
* @method menuLinks
* @return {Array} List of links
*/
Template.layout.menuLinks = function() {
  var links = [
    {
      title:"Home",
      url:"/",
      secure: true
    },
    {
      title:"Register",
      url:"/register",
      secure: true
    },
    {
      title:"Search",
      url:"/search",
      secure: true
    }
    // ,
    // {
    //   title:"Contact",
    //   url:"/contact"
    // }
  ];

  // Business logic for generating list of links to render
  for (var i = links.length - 1; i >= 0; i--) {
    if (links[i].url === Router.current().path) {
      links[i].linkClass = "active";
    }
    if (links[i].secure && !Meteor.user()) {
      links[i].visible = false;
    } else {
      links[i].visible = true;
    }
  };

  return links;
}

/**
* The name of the page currently displayed
*
* @method pageName
* @return {String} Page name
*/
Template.layout.pageName = function() {
  return Router.current().template;
};

/**
* Registration page
*
* @class Register
*/

/**
* Hook to register JQuery UI component
*
* @method rendered
*/
Template.register.rendered = function() {
      $(this.find("#dateImplemented")).datepicker(
        {
            viewMode: 'years',
            format: 'mm-yyyy'
        });
}

/**
* Event listener for .register button - handles form validation, presentation
*
* @method click
*/
Template.register.events({
  "click .register": function(e,t) {
    e.preventDefault();

    var asset = {
      name: t.find('#name').value,
      location: t.find('#location').value,
      department: t.find('#department').value,
      manufacturer: t.find('#manufacturer').value,
      manufacturerPart: t.find('#manufacturer_part_number').value,
      description: t.find('#description').value,
      maintenanceNotes: t.find('#maintenance_notes').value,
      dateImplemented: t.find('#dateImplemented').value,
      imgUrl: ""
    };
    
    if (asset.name &&  // Validate asset data
       asset.location && 
       asset.department && 
       asset.manufacturer && 
       asset.manufacturerPart && 
       asset.description && 
       asset.maintenanceNotes && 
       asset.dateImplemented) {
    
      // Prepare image upload
      var file = t.find('#img').files[0];
      var fd = new FormData();
    
      var key = (new Date).getTime() + '-' + file.name;
      
      // S3 data
      // Policy and signature constructed to enable image arbitrary uploads from clients
      // See http://stackoverflow.com/questions/11240127/uploading-image-to-amazon-s3-with-html-javascript-jquery-with-ajax-request-n
      var data = {
        'key': key,
        'acl': 'public-read',
        'Content-Type': file.type,
        'AWSAccessKeyId': 'AKIAJQAKFQWTLLXJYFDQ',
        'policy': 'eyJleHBpcmF0aW9uIjoiMjAyMC0xMi0wMVQxMjowMDowMC4wMDBaIiwiY29uZGl0aW9ucyI6W3siYnVja2V0IjoibWVzZXJ2eS1sYWIzIn0sWyJzdGFydHMtd2l0aCIsIiRrZXkiLCIiXSx7ImFjbCI6InB1YmxpYy1yZWFkIn0sWyJzdGFydHMtd2l0aCIsIiRDb250ZW50LVR5cGUiLCIiXSxbImNvbnRlbnQtbGVuZ3RoLXJhbmdlIiwwLDUyNDI4ODAwMF1dfQ==',
        'signature': '+8sqfGIpqpL8m0NYirg9INErm+k=',
        'file': file
      };
    
      // Prepare response
      fd.append('key', data.key);
      fd.append('acl', data.acl); 
      fd.append('Content-Type', file.type);      
      fd.append('AWSAccessKeyId', data.AWSAccessKeyId);
      fd.append('policy', data.policy)
      fd.append('signature',data.signature);
      fd.append('file', file);
    
      var xhr = new XMLHttpRequest();
    
      // When the upload is complete, manage document creation
      var uploadComplete = function(evt,b,c,d) {
        asset.imgUrl = 'https://meservy-lab3.s3.amazonaws.com/' + data.key;
        
        Assets.insert(asset, function(r, id) {
          if (id) {
            Router.go('view',{id: id});
          } else {
            alert("Please verify your entry and try again. Missing fields.");
          }
        });
      }
      
      // Attach response hooks
      xhr.upload.addEventListener("progress", uploadProgress, false);
      xhr.addEventListener("load", uploadComplete, false);
      xhr.addEventListener("error", uploadFailed, false);
      xhr.addEventListener("abort", uploadCanceled, false);
    
      xhr.open('POST', 'https://meservy-lab3.s3.amazonaws.com/', true); //MUST BE LAST LINE BEFORE YOU SEND 
    
      xhr.send(fd);
      
    } else {
      alert("Please verify your entry and try again. Missing fields.");
    }
    
    e.preventDefault();
    return false;
  }
});

/**
* View asset view
*
* @class View
*/

/**
* The current asset being viewed, derived from Session variable "assetId"
*
* @method asset
*/
Template.view.asset = function() {
  var asset = Assets.findOne(Session.get("assetId"));
  return asset;
}

/**
* Search assets view
*
* @class Search
*/

/**
* Keystrokes trigger full text search
*
* @method keyup
*/
Template.search.events({
  'keyup .search-query': function(e,t) {
    Session.set('searchQuery', e.target.value);
  }
})

/**
* Dynamic property returning collection data
*
* @method results
* @return {Array} List of result objects
*/
Template.search.results = function() {
  var q = Session.get('searchQuery');
  if (!q || q.length == 0) {
    return Assets.find();
  }

  // Search object across all fields using regex format
  var results = Assets.find({$or: [
      {name: {$regex: '.*'+ q + '.*', $options: 'i'}},
      {location: {$regex: '.*'+ q + '.*', $options: 'i'}},
      {department: {$regex: '.*'+ q + '.*', $options: 'i'}},
      {manufacturer: {$regex: '.*'+ q + '.*', $options: 'i'}},
      {manufacturerPart: {$regex: '.*'+ q + '.*', $options: 'i'}},
      {description: {$regex: '.*'+ q + '.*', $options: 'i'}},
      {maintenanceNotes: {$regex: '.*'+ q + '.*', $options: 'i'}},
      {dateImplemented: {$regex: '.*'+ q + '.*', $options: 'i'}}
    ]});

  return results;
};