/**
* Routing configuration class
*
* @class Router
*/
Router.configure({
  layoutTemplate: 'layout' // Default layout for application
});

/**
* Map of routing endpoints, including home, register, search, and view
*
* @method map
*/
Router.map(function () {
  /**
   * The route's name is "home"
   * The route's template is also "home"
   * The default action will render the home template
   */
  this.route('home', {
    path: '/',
    template: 'home'
  });

  this.route('register', {
    path: '/register',
    template: 'register',
    before: function () { // Security method - requires authenticated user to view
      if (!Meteor.user()) {
        this.render('home');
        this.stop();
      }
    },
  });

  this.route('search', {
    path: '/search',
    template: 'search',
    before: function () { // Security method - requires authenticated user to view
      if (!Meteor.user()) {
        this.render('home');
        this.stop();
      }
    },
  });

  // this.route('contact', {
  //   path: '/contact',
  //   template: 'contact'
  // });

  this.route('view', {
    path: '/view/:id',
    action: function() { // Render page based on asset presently selected
      Session.set("assetId", this.params.id);
      this.render();
    }
  })

});