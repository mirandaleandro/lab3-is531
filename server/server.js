/** Setup originating email to verified SES email */
Meteor.startup(function() {
  Accounts.emailTemplates.from = 'me@brentjanderson.com';
});