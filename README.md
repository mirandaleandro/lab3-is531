# Group project 2 - Meservy IS 530

To run this project, you must [Install Meteor](http://www.meteor.com) and then [Meteorite](http://github.com/oortcloud/meteorite) and then from a command line:

    `cd lab3-is531`
    `mrt install`
    `meteor`

It should just work. To compile documentation, you need to install yuidoc....
    `npm install -g yuidoc`

And then from the project folder run...

    `yuidoc .`

This will then generate documentation in the private/docs directory.
